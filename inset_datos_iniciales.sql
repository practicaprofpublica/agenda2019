INSERT INTO `tipo_dni` (`codigo`, `descripcion`) VALUES
(1, 'DNI'),
(2, 'Cedula');

INSERT INTO `personas` (`dni`, `nombre`, `apellido`, `tipo_dni_codigo`) VALUES
(26564566, 'pepe', 'argento', 1),
(31806721, 'Emmanuel', 'Funes', 1),
(124546543, 'Rupertaa', 'diaz', 2),
(159872126, 'Brat', 'Semsomp', 1),
(223434232, 'Franquito', 'Ferrada', 1),
(234234234, 'Alam', 'Brado', 2),
(234234456, 'Matias', 'Lohizo', 1),
(234235426, 'Jose', 'Perez', 1),
(234554545, 'Leonel', 'Rolon', 1),
(234564343, 'Anahi', 'Britos', 1),
(318999898, 'Norma', 'Idelfonso', 1),
(334534533, 'Carolina', 'Delmonte', 1),
(343457634, 'Empedocles', 'Gutierrez', 1),
(345345232, 'Gladis', 'Velazquez', 1),
(345345333, 'Violeta', 'Rojo', 1),
(345345345, 'Ada', 'Fernandez', 1),
(345345455, 'Nicolas', 'Barrios', 1),
(345356345, 'Emmanuel', 'Funes', 1),
(20606791, 'Marcelo', 'Fajardo', 1),
(349345345, 'Elmos', 'Quito', 2),
(349945345, 'Adriana', 'Canabiri', 2),
(365545456, 'Alejandro', 'Magno', 1),
(435345345, 'Daniel', 'Lewis', 1),
(456456444, 'Elver', 'Gachica', 1),
(456456456, 'Rosa', 'Meltrozo', 1),
(456564564, 'Debora', 'Meltrozo', 1),
(534534534, 'Gabriela', 'Quiroga', 1),
(545454534, 'Elver', 'Galarga', 2),
(654544544, 'Soledad', 'Solaro', 1),
(655645454, 'Danilo', 'Rossi', 1),
(687654433, 'Federico', 'Perassi', 1),
(765675644, 'Elvi', 'Olado', 1),
(2147483647, 'Patricia', 'Sousa', 2);



INSERT INTO `categoria_usuarios` (`id`, `nombre`, `descripcion`, `codigo`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrador', 1, '2018-10-26 02:15:43', '2018-10-26 02:15:43'),
(2, 'user', 'Usuario', 40, '2018-10-26 02:15:43', '2018-10-26 02:15:43'),
(3, 'Jefe', 'Jefe', 20, '2018-10-26 02:15:43', '2018-10-26 02:15:43'),
(4, 'operario', 'operario', 30, '2018-10-26 02:15:43', '2018-10-26 02:15:43');


INSERT INTO `usuario` (`id_usuario`, `email`, `clave`, `create_time`, `personas_dni`,`categoria_usuarios_id`) VALUES
(1, 'motiusdotnet@gmail.com', 'verde', '2016-10-22 00:00:00', 20606791, 1);


INSERT INTO `prioridad` (`cod_pr`, `tipo`) VALUES
(9, 'Urgente'),
(20, 'Prioritaria'),
(30, 'Normal');


INSERT INTO `datos_personales` (`id_dp`, `domicilio`, `telefono1`, `telefono2`, `email_personal`, `telegram`, `foto`, `personas_dni`) VALUES
(4, 'aca nomas', '4545345', '767576575', 'ja@gmail.cm', NULL, NULL, 20606791),
(5, 'lejos', '4535345', '76555554', 'fvfdg@ggg', NULL, NULL, 20606791),
(6, 'aca', '5756546', '', 'marce@motius.net', '', NULL, 20606791),
(7, 'Aca nomas', '6668761287', '234234234', 'funesomar@gmail.com', 'No tiene ', NULL, 20606791),
(8, 'No tan cerca', '123331223', '234234234', 'fede@gmailo', 'vaya usted a saber', NULL, 20606791),
(9, 'cerca de aca', '99955588', '123456', 'prueba@email', '', NULL, 20606791),
(10, 'cerca de aca155', '66955588', '923458', 'prueba1@email', '', NULL, 20606791),
(11, 'lejos de aca', '9995959588', '15656456', 'prueba2@email', '', NULL, 20606791),
(12, 'dfsdf', 'dsfsd', 'dsfsd', 'dsfsdf', '', NULL, 20606791),
(13, 'aca', '674654546748', '234234', 'funesomar@gmail.com', 'vaya usted a saber', NULL, 20606791),
(15, '', '', '', '', '', NULL, 20606791),
(16, 'Sarmiento 78', '', '', 'mmmm@gmail.com', '', NULL, 20606791);


