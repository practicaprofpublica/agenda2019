<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Programación Orientada a Objetos</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<link rel="stylesheet" href="bootstrap/css/custom.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Listado de  <b>Pacientes</b></h2></div>
                    <div class="col-sm-4">
                        <a href="?menu=create" class="btn btn-info add-new"><i class="fa fa-plus"></i> Agregar Paciente</a>
                    </div>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nombres</th>
                        <th>Teléfono</th>
                        <th>Dirección</th>
						            <th>E-mail</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
				<?php

				require_once ('modelo/model_paciente.php');
        /*echo'<script type="text/javascript">
          alert("requiere modelo paciente");

          </script>';*/
				$pacientes = new Model_Paciente();
				$listado=$pacientes->read();
				?>
                <tbody>
				<?php
					while ($row=mysqli_fetch_object($listado)){
						$id=$row->id;
						$nombres=$row->nombres." ".$row->apellidos;
						$telefono=$row->telefono;
						$direccion=$row->direccion;
						$email=$row->correo_electronico;

				?>
					<tr>
                        <td><?php echo $nombres;?></td>
                        <td><?php echo $telefono;?></td>
                        <td><?php echo $direccion;?></td>
						            <td><?php echo $email;?></td>
                        <td>

                  <a href="?menu=update&id=<?php echo $id;?>"
                    class="edit" title="Editar" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                  <a href="?menu=delete&id=<?php echo $id;?>"
                    class="delete" title="Eliminar" data-toggle="tooltip"
                    onclick="return confirm('esta seguro de eliminar <?php echo $nombres;?>?')">
                      <i class="material-icons">&#xE872;</i></a>
                        </td>
          </tr>
				<?php
      }
				?>



                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
